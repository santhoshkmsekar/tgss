package base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.apache.poi.ss.usermodel.Cell;

public class BaseClass extends BaseTest {

	public static Logger log = LogManager.getLogger(BaseClass.class);

	public static void clickelement(WebElement element) {
		try {
			log.info("Clicking the element" + " " + element);
			element.click();
		} catch (NoSuchElementException e) {
			log.info("No such element found in the page");
		}
	}

	public static String getdata(Integer SHEET, Integer ROW, Integer CELL) {
		String cellval = null;
		try {
			FileInputStream fis = new FileInputStream("./Testdata/Data.xlsx");
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			XSSFSheet sheet = workbook.getSheetAt(SHEET);
			Row row = sheet.getRow(ROW);
			Cell cell = row.getCell(CELL);
			cellval = cell.getStringCellValue();

		} catch (FileNotFoundException e) {
			log.info("No file given in the location");
			e.printStackTrace();
		} catch (IOException e) {
			log.info("No input found in the given excel file");
			e.printStackTrace();
		}
		return cellval;

	}

	public static void sendkeyds(WebElement element, String value) {
		try {
			element.sendKeys(value);
		} catch (NoSuchElementException e) {
			log.info("No such element found in the page");
			e.printStackTrace();
		}

	}
}
