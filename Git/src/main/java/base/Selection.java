package base;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Selection extends BaseTest {
	public static WebDriver ldriver;
	public static Logger log = LogManager.getLogger(Selection.class);
	public static WebDriver browserSelection(String browser_name, WebDriver driver, String app_url) {

		if (browser_name.equals("Chrome")) {
			System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
			log.info("Initializing the chrome driver");
			driver = new ChromeDriver();
			log.info("Initialization completed");
		} else if (browser_name.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./Drivers/geckodriver.exe");
			log.info("Initializing the firefox driver");
			driver = new FirefoxDriver();
			log.info("Initialization completed");
		} else {
			log.info("We do not support this browser");
		}

		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.get(app_url);
		ldriver = driver;
		return driver;
	}
}
