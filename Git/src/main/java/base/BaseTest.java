package base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class BaseTest {
	public static WebDriver driver;
	public Properties pro;
	public Logger log = LogManager.getLogger(BaseTest.class);

	public BaseTest() {
		try {
			FileInputStream reader = new FileInputStream("./src/main/java/properties/Config.properties");
			pro = new Properties();
			pro.load(reader);
		} catch (FileNotFoundException e) {
			log.info("File not found in your location");
		} catch (IOException e) {
			log.info("No input in the given Properties file");
		}
	}

	@Test
	public void initialization() {
		log.info("Selecting the Browser");
		Selection.browserSelection(pro.getProperty("BROWSER_NAME"), driver, pro.getProperty("APP_URL"));
		driver = Selection.ldriver;
		System.out.println("TestData"+BaseClass.getdata(0, 0, 0));	
	}

	

	@AfterTest
	public void close_browser() {
		log.info("Closing the browser");
		driver.quit();
		log.info("Closed the browser successfully");
	}

}
